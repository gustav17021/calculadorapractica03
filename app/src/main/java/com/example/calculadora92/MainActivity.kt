package com.example.calculadora92

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.view.View
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var txtUser: EditText
    private lateinit var txtPass: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtUser=findViewById(R.id.txtUsuario)
        txtPass=findViewById(R.id.txtPass)
    }
    fun ingresar(v: View){
        txtUser=findViewById(R.id.txtUsuario)
        txtPass=findViewById(R.id.txtPass)
        if (txtUser.text.toString()=="Armenta" && txtPass.text.toString()=="AG"){
            val intent= Intent(this,CalculadoraActivity::class.java)
            val message =txtUser.text.toString()
            intent.putExtra(Companion.EXTRA_MESSAGE,message)
            startActivity(intent)
            txtUser.setText("")
            txtPass.setText("")
        }
        else{
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show()
        }
    }
    fun salir(v:View){
        this.finish();
    }

    companion object {
        const val EXTRA_MESSAGE="com.example.MainActivity.MESSAGE"
    }
}
