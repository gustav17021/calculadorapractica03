package com.example.calculadora92
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

private lateinit var btnSumar:Button
private lateinit var btnRestar:Button
private lateinit var btnMulti:Button
private lateinit var btnDiv:Button
private lateinit var btnLimpiar:Button
private lateinit var btnRegresar:Button
private lateinit var lblUsuario:TextView
private lateinit var lblResultado:TextView
private lateinit var txtUno:EditText
private lateinit var txtDos:EditText

private var calculadora=Calculadora(0,0)
class CalculadoraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComp()
        /*
        var datos=intent.extras
        var usuario=datos!!.getString("usuario")*/
        val intent=getIntent()
        val usuario=intent.getStringExtra(MainActivity.EXTRA_MESSAGE)
        lblUsuario.text=usuario.toString()
    }
    private fun iniciarComp(){

        btnSumar=findViewById(R.id.btnSumar)
        btnRestar=findViewById(R.id.btnRestar)
        btnMulti=findViewById(R.id.btnMultiplicar)
        btnDiv=findViewById(R.id.btnDividir)
        btnLimpiar=findViewById(R.id.btnLimpiar)
        btnRegresar=findViewById(R.id.btnRegresar)
        lblUsuario=findViewById(R.id.lblUsuario)
        lblResultado=findViewById(R.id.lblResultado)
        txtUno=findViewById(R.id.txtNum1)
        txtDos=findViewById(R.id.txtNum2)
    }
    fun vacio(): Boolean {
        if (txtUno.text.toString()==""&& txtDos.text.toString()==""){
            return true
        }
        else {return false}
    }
    fun sumar(v: View){
        if(vacio()){
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show()
        }
        else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.suma()
            lblResultado.text = "Resultado: " + total.toString()
        }
    }
    fun resta(v:View){
        if(vacio()){
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show()
        }
        else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.resta()
            lblResultado.text = "Resultado: " + total.toString()
        }
    }
    fun multi(v:View){
        if(vacio()){
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show()
        }
        else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.multiplicacion()
            lblResultado.text = "Resultado: " + total.toString()
        }
    }
    fun div(v:View){
        if(vacio()){
            Toast.makeText(this, "Campos incorrectos", Toast.LENGTH_SHORT).show()
        }
        else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.division()
            lblResultado.text = "Resultado: " + total.toString()
        }
    }
    fun limpiar(v:View){
        lblResultado.text="Resultado"
        txtUno.setText("")
        txtDos.setText("")
    }
    fun regresar(v:View){
        var confirmar=AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar a la pagina principal")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,wich->}
        confirmar.show()

    }
}
